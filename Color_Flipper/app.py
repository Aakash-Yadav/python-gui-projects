

from tkinter import *
import tkinter.font as font
from random import choice 
from main import  return_hex 

class Gui(object):

    def __init__(self):
        self.root = Tk()
        self.root.geometry("300x300")
        self.root.title("Random color")
        self.root.config(bg="#282a36");
        buttonFont = font.Font(family='Helvetica', size=16, weight='bold')
        self.Button = Button(self.root,bd=4,width=8,font=buttonFont,bg="#50fa7b",fg='#44475a',
                text="change",command=self.change_color)
        self.Button.place(relx=0.5, rely=0.5, anchor=CENTER)

    def run(self):
        self.root.mainloop();
        return 1;

    def change_color(self):
        color = return_hex()
        self.root.config(bg=color);
        self.Button.config(text=color);
        return 1 



if __name__ == "__main__":
    app = Gui()
    app.run()
