from setuptools import setup
from os import listdir , system 
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("main.pyx")
)

for i in listdir('.'):
    if i=='build' or i=='main.c':
        system('rm -rf build  main.c')
