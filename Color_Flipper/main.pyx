
from string import hexdigits
from random import choice 

cdef random_hash_generator():
    cdef unsigned int x;
    return ( "#%s"%''.join([choice(hexdigits) for x in range(6)]))

cpdef return_hex():
    return random_hash_generator()




